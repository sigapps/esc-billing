<?php
namespace Esc\Billing;

use Illuminate\Database\Eloquent\Model;


class Purchase extends Model{
    protected $table = 'purchases';
    protected $fillable = [
    ];


    public function shop() {
        return $this->belongsTo(\App\Shop::class, 'shop_id');
    }

    public function discount() {
        return $this->belongsTo(\Esc\Billing\Discount::class, 'discount_id');
    }

    public function applyDiscountCode($code) {
        $error = false;
        $newPrice = \Esc\Billing\Discount::tryUse($code, $this->amount, $error, 'purchase', $this->shop);
        if ($error) {
            return $error;
        }
        $this->discount()->associate(\Esc\Billing\Discount::findByCode($code));
        $this->amount = $newPrice;
        $this->save();
        return true;
    }


    public function createInShopify() {
        try {
            $api = $this->shop->getAPI();
            $res = $api->call('post', '/admin/application_charges.json', [
                'application_charge' => [
                    'name' => $this->name,
                    'price' => $this->amount / 100,
                    'return_url' => \URL::action('\Esc\Billing\Http\Controllers\ConfirmationController@confirmPurchase'),
                    'test' => $this->test
                ]
            ]);

            $charge = $res->application_charge;
            if (!$charge || $charge->status != 'pending') {
                return false;
            }

            $this->application_charge_id = $charge->id;
            $this->status = $charge->status;
            $this->confirmation_url = $charge->confirmation_url;
            $this->save();

            return true;

        } catch (\Exception $ex) {
            throw $ex;
            return false;
        }
    }

    public function process() {
        try {
            $api = $this->shop->getAPI();
            $res = $api->call('get', '/admin/application_charges/'.$this->application_charge_id.'.json');
            $charge = $res->application_charge;

            $this->status = $charge->status;

            if ($this->status == 'accepted') {
                $this->paid = true;
                $this->save();

                return redirect()->to($this->redirect)->with('success', 'Payment successful.');
            }

            $this->save();
            return redirect()->to($this->redirect)->with('error', 'The payment was cancelled.');
        } catch (\Exception $ex) {

            return redirect()->to($this->redirect)->with('error', 'An error occurred.');
        }
    }

    public function canPay() {
        return $this->status == 'pending';
    }

    public function redirect() {
        return \Response::make('<!DOCTYPE html><html><head></head><body><script type="text/javascript"> if (window.top == window.self) { window.top.location.href = "'.$this->confirmation_url.'"; } else { message = JSON.stringify({ message: "Shopify.API.remoteRedirect", data: { location: "'.$this->confirmation_url.'" } }); window.parent.postMessage(message, "https://'.$this->shop->shop_domain.'"); } </script></body></html>');

    }


}
