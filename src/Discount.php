<?php
namespace Esc\Billing;

use Illuminate\Database\Eloquent\Model;


class Discount extends Model{
    const QUANTITY_MODE_UNLIMITED           = 'unlimited';
    const QUANTITY_MODE_ONCE_PER_CUSTOMER   = 'once_per_customer';
    const QUANTITY_MODE_PER_USE             = 'per_use';

    const AMOUNT_MODE_PERCENTAGE            = 'percentage';
    const AMOUNT_MODE_FIXED                 = 'fixed';

    const ROUNDING_MODE_DISABLED            = 'disabled';
    const ROUNDING_MODE_DOLLAR              = 'dollar';

    const SUBJECT_MODE_SUBSCRIPTION         = 'subscription';
    const SUBJECT_MODE_PURCHASE             = 'purchase';
    const SUBJECT_MODE_BOTH                 = 'both';

    const ERROR_ALREADY_USED                = 'That discount code has already been used the maximum number of times.';
    const ERROR_NOT_FOUND                   = 'That discount code was not found.';
    const ERROR_WRONG_SUBJECT_TYPE          = 'That discount code cannot be used on this type of purchase.';


    protected $table = 'discounts';


    public function subscriptions() {
        return $this->hasMany(\Esc\Billing\Subscription::class, 'discount_id');
    }

    public function purchases() {
        return $this->hasMany(\Esc\Billing\Purchase::class, 'discount_id');
    }



    public static function findByCode($code) {
        return Discount::where('code', $code)->first();
    }


    public static function check($code, &$error) {
        $discount = static::findByCode($code);
        if (!$discount) {
            $error = self::ERROR_NOT_FOUND;
            return false;
        }

        if ($discount->quantity_remaining == 0) {
            $error = self::ERROR_ALREADY_USED;
            return false;
        }

        if ($discount->quantity_mode == self::QUANTITY_MODE_ONCE_PER_CUSTOMER) {
            if (!$shop) {
                $error = self::ERROR_ALREADY_USED;
                return false;
            }

            $sub = $discount->subscriptions()->where('shop_id', $shop->id)->first();
            $purchase = $discount->purchases()->where('shop_id', $shop->id)->first();
            if ($sub || $purchase) {
                $error = self::ERROR_ALREADY_USED;
                return false;
            }
        }

        if ($discount->subject_mode != self::SUBJECT_MODE_BOTH && $discount->subject_mode != $type) {
            $error = self::ERROR_WRONG_SUBJECT_TYPE;
            return false;
        }

        return true;
    }


    public static function tryUse($code, $oldPrice, &$error, $type, $shop = null) {
        $discount = static::findByCode($code);
        if (!$discount) {
            $error = self::ERROR_NOT_FOUND;
            return false;
        }

        if ($discount->quantity_remaining == 0) {
            $error = self::ERROR_ALREADY_USED;
            return false;
        }

        if ($discount->quantity_mode == self::QUANTITY_MODE_ONCE_PER_CUSTOMER) {
            if (!$shop) {
                $error = self::ERROR_ALREADY_USED;
                return false;
            }

            $sub = $discount->subscriptions()->where('shop_id', $shop->id)->first();
            $purchase = $discount->purchases()->where('shop_id', $shop->id)->first();
            if ($sub || $purchase) {
                $error = self::ERROR_ALREADY_USED;
                return false;
            }
        }

        if ($discount->subject_mode != self::SUBJECT_MODE_BOTH && $discount->subject_mode != $type) {
            $error = self::ERROR_WRONG_SUBJECT_TYPE;
            return false;
        }


        $newPrice = $oldPrice;
        if ($discount->amount_mode == self::AMOUNT_MODE_FIXED) {
            $newPrice -= $discount->amount_mode;
            if (!$newPrice < 0) {
                $newPrice = 0;
            }
        } else {
            $newPrice *= 1.0 - ($discount->percentage / 100.0);
        }

        if ($discount->rounding_mode == self::ROUNDING_MODE_DOLLAR) {
            $newPrice = intval(round($newPrice / 100) * 100);
        }

        $newPrice = intval($newPrice);

        $discount->quantity_remaining--;
        $discount->save();

        $error = false;
        return $newPrice;
    }

}
