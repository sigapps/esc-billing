<?php
namespace Esc\Billing;

use Illuminate\Database\Eloquent\Model;


class Plan extends Model {
    const TOKEN_MODE_NEVER = 'never';
    const TOKEN_MODE_1ST = 'startofmonth';
    const TOKEN_MODE_30_DAYS = 'standard';
    
    const TRIAL_TYPE_DISABLED = 'disabled';
    const TRIAL_TYPE_PER_ACCOUNT = 'account';
    const TRIAL_TYPE_PER_PLAN = 'plan';
    const TRIAL_TYPE_EVERY_TIME = 'always';
    
    protected $table = 'plans';
    protected $fillable = [
        'name','amount','trial_days','trial_type',
        'permissions','terms','tokens_per_month',
        'capped_amount','refill_mode'
    ];
    protected $casts = [
        'permissions' => 'array'
    ];
    
    public function shops() {
        return $this->hasMany(\App\Shop::class, 'plan_id');
    }
}