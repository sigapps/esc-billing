<?php
namespace Esc\Billing\Http\Controllers;

use Illuminate\Http\Request;


class ConfirmationController extends Controller {
    public function confirmPurchase(Request $request) {
        $purchase = \Esc\Billing\Purchase::where('application_charge_id', $request->get('charge_id'))->first();
        return $purchase->process();
    }
    
    public function confirmSubscription(Request $request) {
        $sub = \Esc\Billing\Subscription::where('shopify_charge_id', $request->get('charge_id'))->first();
        return $sub->process();
    }
}