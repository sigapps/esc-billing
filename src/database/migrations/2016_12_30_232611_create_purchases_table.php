<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('purchases', function($table) {
           $table->increments('id');
           $table->timestamps();
           
           $table->string('shop_type');
           $table->integer('shop_id');
           
           $table->string('name');
           $table->string('amount');
           $table->string('redirect');
           $table->boolean('test');
           $table->boolean('paid');
           
           $table->string('status');
           
           $table->string('confirmation_url')->nullable();
           $table->string('application_charge_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::drop('purchases');
    }
}
