<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('discounts', function($table) {
            $table->increments('id');
            $table->timestamps();

            $table->enum('quantity_mode', [
                'unlimited',
                'once_per_customer',
                'per_use'
            ]);

            $table->enum('amount_mode', [
                'percentage',
                'fixed'
            ]);

            $table->enum('rounding_mode', [
                'disabled',
                'dollar',
            ]);

            $table->enum('subject_mode', [
                'subscription',
                'purchase',
                'both'
            ]);

            $table->string('name')->nullable();
            $table->string('code');
            $table->integer('fixed_amount');
            $table->integer('percentage');
            $table->integer('total_quantity')->default(-1);
            $table->integer('quantity_remaining')->default(-1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::drop('discounts');
    }
}
